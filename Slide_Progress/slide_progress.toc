\beamer@sectionintoc {1}{Background}{3}{0}{1}
\beamer@sectionintoc {2}{Objectives}{5}{0}{2}
\beamer@sectionintoc {3}{Flux extraction}{6}{0}{3}
\beamer@subsectionintoc {3}{1}{Schematics of limb $\gamma $-ray production}{6}{0}{3}
\beamer@subsectionintoc {3}{2}{Data set}{7}{0}{3}
\beamer@subsectionintoc {3}{3}{Calculation}{8}{0}{3}
\beamer@sectionintoc {4}{Analysis}{19}{0}{4}
\beamer@subsectionintoc {4}{1}{Kachelrie$\beta $ and Ostapchenko model}{19}{0}{4}
\beamer@subsectionintoc {4}{2}{Optimization}{20}{0}{4}
\beamer@subsectionintoc {4}{3}{Results}{22}{0}{4}
\beamer@subsectionintoc {4}{4}{Monte Carlo simlation}{24}{0}{4}
\beamer@sectionintoc {5}{To do list}{28}{0}{5}
\beamer@sectionintoc {7}{Reference}{31}{0}{6}
