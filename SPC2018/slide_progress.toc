\beamer@sectionintoc {1}{Objectives}{3}{0}{1}
\beamer@sectionintoc {2}{Flux extraction}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Schematics of limb $\gamma $-ray production}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Data set}{5}{0}{2}
\beamer@subsectionintoc {2}{3}{Calculation}{6}{0}{2}
\beamer@sectionintoc {3}{Analysis}{11}{0}{3}
\beamer@subsectionintoc {3}{1}{Kachelrieß and Ostapchenko model}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Optimization}{12}{0}{3}
\beamer@subsectionintoc {3}{3}{Results}{13}{0}{3}
\beamer@sectionintoc {4}{To do list}{15}{0}{4}
\beamer@sectionintoc {6}{Reference}{16}{0}{5}
